package com.bignerdranch.mrmadom2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

public class ContactViewModel extends ViewModel {

    private MutableLiveData<List<Contact>> contactsLiveData;
    private MutableLiveData<String> filterLiveData;
    private LiveData<List<Contact>> filteredContactsLiveData;

    public ContactViewModel() {
        contactsLiveData = new MutableLiveData<>();
        // TODO add dummy data
        List<Contact> initialContacts = new ArrayList<>();
        initialContacts.add(new Contact("duki", "mhm", "123443", "duki@raf.rs"));
        initialContacts.add(new Contact("cabi", "aha", "321322", "cabi@raf.rs"));
        initialContacts.add(new Contact("draza", "jea", "403892384", "draza@raf.rs"));
        initialContacts.add(new Contact("rudi", "isto", "5325235", "bolje ne pitaj"));

        for (int i=0; i<100; i++) {
            initialContacts.add(new Contact("Ime " + i, "Prezime " + i, "321-322", "str@s.no"));
        }
        contactsLiveData.setValue(initialContacts);
        filterLiveData = new MutableLiveData<>();
        filterLiveData.setValue("");
        filteredContactsLiveData = Transformations.switchMap(filterLiveData, prefix ->
                Transformations.map(contactsLiveData, contacts ->
                        contacts.stream()
                                .filter(c -> c.startsWith(prefix))
                                .collect(Collectors.toList())
                )
        );
    }

    public void setContacts(List<Contact> contacts) {
        contactsLiveData.setValue(contacts);
    }

    public List<Contact> getAllContacts() {
        return contactsLiveData.getValue();
    }

    public void setFilter(String filter) {
        filterLiveData.setValue(filter);
    }

    public LiveData<List<Contact>> getContactsLiveData() {
        return filteredContactsLiveData;
    }
}
