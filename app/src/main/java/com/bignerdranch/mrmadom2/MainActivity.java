package com.bignerdranch.mrmadom2;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private ContactViewModel viewModel;

    private RecyclerView contactsRecyclerView;

    private EditText filterEditText;

    private EditText addEditText;
    private Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        initRecyclerView();

        initFilter();
        initAdd();
    }

    private void initRecyclerView() {
        contactsRecyclerView = findViewById(R.id.contact_recycler_view);
        contactsRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        ContactAdapter adapter = new ContactAdapter();
        adapter.setUpViewModel(viewModel, this);
        contactsRecyclerView.setAdapter(adapter);

        applyMargin(contactsRecyclerView);
    }

    private void applyMargin(RecyclerView recyclerView) {
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int margin = 10;
                outRect.top = margin;
                outRect.bottom = margin;
                outRect.left = margin;
                outRect.right = margin;
            }
        });
    }

    private void initFilter() {
        filterEditText = findViewById(R.id.filter_edit_text);
        filterEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setFilter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initAdd() {
        addEditText = findViewById(R.id.add_edit_text);

        addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(this::handleAddButtonClick);
    }

    private void handleAddButtonClick(View v) {
        String fullName = addEditText.getText().toString();
        String[] nameParts = fullName.split(" ");
        String firstName = nameParts[0];
        String lastName = nameParts[1];

        Contact contact = new Contact(firstName, lastName, "dodat naknadno", "dodat naknadno");

        List<Contact> newContacts = new ArrayList<>();
        newContacts.addAll(viewModel.getAllContacts());
        newContacts.add(contact);

        viewModel.setContacts(newContacts);
    }
}
