package com.bignerdranch.mrmadom2;

import java.util.List;

import androidx.recyclerview.widget.DiffUtil;

public class ContactDiffUtilCallback extends DiffUtil.Callback {

    private List<Contact> oldContacts;
    private List<Contact> newContacts;

    public ContactDiffUtilCallback(List<Contact> oldContacts, List<Contact> newContacts) {
        this.oldContacts = oldContacts;
        this.newContacts = newContacts;
    }

    @Override
    public int getOldListSize() {
        return oldContacts.size();
    }

    @Override
    public int getNewListSize() {
        return newContacts.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        String oldEmail = oldContacts.get(oldItemPosition).getEmail();
        String newEmail = newContacts.get(newItemPosition).getEmail();

        return oldEmail.equals(newEmail);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
