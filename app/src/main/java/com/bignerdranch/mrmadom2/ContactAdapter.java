package com.bignerdranch.mrmadom2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactHolder> {
    private static final String TAG = "CONTACT_ADAPTER";

    public static class ContactHolder extends RecyclerView.ViewHolder {
        TextView fullNameTv;
        TextView phoneNumberTv;
        TextView emailTv;

        ContactHolder(@NonNull View itemView) {
            super(itemView);
            fullNameTv = itemView.findViewById(R.id.full_name_tv);
            phoneNumberTv = itemView.findViewById(R.id.phone_number_tv);
            emailTv = itemView.findViewById(R.id.email_tv);
        }
    }


    private List<Contact> contacts = new ArrayList<>();

    public ContactAdapter() {

    }

    public void setUpViewModel(ContactViewModel viewModel, LifecycleOwner lifecycleOwner) {
        viewModel.getContactsLiveData()
                .observe(lifecycleOwner, this::setContacts);
    }

    public void setContacts(List<Contact> newContacts) {
        ContactDiffUtilCallback callback = new ContactDiffUtilCallback(contacts, newContacts);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(callback);
        contacts.clear();
        contacts.addAll(newContacts);
        result.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.contact_card, parent, false);
        return new ContactHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactHolder holder, int position) {
        setContent(holder, contacts.get(position));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    private void setContent(ContactHolder holder, Contact contact) {
        Log.d(TAG, "setContent: Evo tu po kuci malo");
        holder.fullNameTv.setText(contact.getFirstName() + " " + contact.getLastName());
        holder.emailTv.setText(contact.getEmail());
        holder.phoneNumberTv.setText(contact.getPhoneNumber());
    }
}
